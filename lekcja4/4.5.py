#!/usr/bin/python
# Kamil Zagula
#
#

def odwracanie(L, left, right):
	L[left:right] = reversed(L[left:right])

lista = [1, 2, 3, 4, 5, 6, 7, 8]
odwracanie(lista, 3, 6)
print lista
