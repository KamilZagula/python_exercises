#!/usr/bin/python
# Kamil Zagula
#
# Program tworzy 'wyraz' z pierwszych i ostatnich liter wyrazow stringu zapisanego w zmiennej line

line = 'jaki sobie tam string'

firstLetters = ''
lastLetters = ''
for word in line.split():
	firstLetters += word[0]
	lastLetters += word[len(word)-1]

print firstLetters
print lastLetters
