#!/usr/bin/python
# Kamil Zagula
#
# Program wyswietla napis stworzony z kolejnych liczb zawartych w tablicy L

import sys

L = (1, 321, 123, 566, 213, 24, 43, -21, -214, 24)
output = ''
for number in L:
	output += repr(abs(number))
print output
