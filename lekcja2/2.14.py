#!/usr/bin/python
# Kamil Zagula
#
# Program znajduje najdluzszy wyraz w napisie line i wyswietla go wraz z jego dlugoscia

line = 'jakis tam sobie string najdluzszy wyraz'
print 'Najdluzszy wyraz to: \'%s\' ma dlugosc = %d' % (max(line.split(), key=len), len(max(line.split(), key=len)))