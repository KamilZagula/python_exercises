#!/usr/bin/python
# Kamil Zagula
#
# Program zamienia ciag znakow 'GvR' na 'Guido van Rossum' w tekscie zawartym w zmiennej line

line = 'test ze stringiem GvR test saf saf GvR test2 a tutajGvR'
print line.replace('GvR', 'Guido van Rossum')
