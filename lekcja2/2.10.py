#!/usr/bin/python
#
# Program wyswietla liczbe wyrazow w napisie. 
# Odczytuje napis z pliku podanego w argumencie wywolania programu.


import sys

fp = open(sys.argv[0], 'r')
fileContent = fp.read()
print len(fileContent.split())
