#!/usr/bin/python
# Kamil Zagula
#
# Program zlicza laczna dlugosc wyrazow w napisie line

line = 'jakis tam sobie string'

wordLengths = map(len, line.split())
print sum(wordLengths)
