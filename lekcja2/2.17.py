#!/usr/bin/python
# Kamil Zagula
#
# Program sortuje wyrazy w napisie zapisanym w zmiennej line raz alfabetycznie a raz wzgledem dlugosci

line = 'wyraz do posortowania, raz alfabetycznie, a raz pod wzgledem dlugosci'
print 'Alfabetycznie:'
print sorted(line.split())
print 'Wzgledem dlugosci:'
print sorted(line.split(), key=len)
