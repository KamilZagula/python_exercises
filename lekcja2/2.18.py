#!/usr/bin/python
# Kamil Zagula
#
# Program zlicza liczbe zer w liczbie zapisanej w zmiennej number

number = 2104000241042142010421004210000
print 'Liczba zer w numerze %d = %d' % (number, repr(number).count('0'))
