#!/usr/bin/python
# Kamil Zagula
#
# Program rozdziela kazda litere wyrazu 'word' znakiem podkreslenia

import sys

word = 'word'
for index, letter in enumerate(word):
	if index != len(word)-1:
		sys.stdout.write(letter + '_')
	else:
		sys.stdout.write(letter)
print
