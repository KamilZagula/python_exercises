def lider_py(L, left, right):
	D = {}
	for i in range(left, right+1):
		D[L[i]] = D.get(L[i], 0) + 1
	item = max(D, key=D.get)
	if D[item] > (right-left+1)/2:
		for i in range(left, right+1):
			if L[i] == item:
				return item
	else:
		return None

print lider_py([3, 3, 1, 3], 0, 3)