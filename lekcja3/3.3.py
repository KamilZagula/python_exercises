#!/usr/bin/python
# Kamil Zagula
#
# Program wypisuje wszystkie liczby od 0 do 30, ktore nie sa podzielne przez 3

for i in range(0,30):
	if i % 3 != 0:
		print i,
