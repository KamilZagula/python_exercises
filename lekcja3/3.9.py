#!/usr/bin/python
# Kamil Zagula
#
# Program wyswietlajacy sumy liczb z sekwencji zawartych w glownej sekwencji

mainSequence = [[],[4],(1,2),[3,4],(5,6,7)]

output = []
index = 0
for subSequence in mainSequence:
	output.append(0)
	for j in subSequence:
		output[index] += j
	index += 1
print output
