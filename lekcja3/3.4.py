#!/usr/bin/python
# Kamil Zagula
#
# Program wypisujacy liczbe wpisana przez uzytkownika i jej 3 potege do czasu wpisania 'stop' przez uzytkownika

while True:
	user_input = raw_input()
	try:
		value = float(user_input)
		print value, pow(value, 3)
	except ValueError:
		if user_input == 'stop':
			break
		else:
			print 'Podana wartosc nie jest liczba'
