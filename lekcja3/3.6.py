#!/usr/bin/python
# Kamil Zagula
#
# Program wyswietlajacy tabele o zadanej ilosci kolumn i wierszy

import sys

if len(sys.argv) != 3:
	print 'Wywolanie programu:', __file__, 'ilosc_wierszy ilosc_kolumna'
	sys.exit()
else:
	try:
		rows = int(sys.argv[1])
		columns = int(sys.argv[2])
		if columns < 1 or rows < 1:
			raise ValueError('Jedna z podanych wartosci nie jest liczba naturalna wieksza od 0')
	except ValueError:
		print 'Podany argument musi byc liczba naturalna'
		sys.exit()

rowsDivider = ('---').join("+" * (columns+1))
rowsContent = ('   ').join("|" * (columns+1))

output = ''
for i in range(0, rows):
	output += rowsDivider + '\n' + rowsContent + '\n'
output += rowsDivider
print output

#output = ''
#for j in range(0, rows*2+1):
#	if j % 2 == 0:
#		for i in range(0, columns):
#			output += '+---'
#		output += '+\n'
#	else:
#		for i in range(0, columns):
#			output += '|   '
#		output += '|\n'
#print output
