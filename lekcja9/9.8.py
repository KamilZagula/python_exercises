def max_btree(top):
    if top is None:
        raise ValueError("Drzewo jest puste!")
    else:
        while top.right is not None:
            top = top.right
        return top


def min_btree(top):
    if top is None:
        raise ValueError("Drzewo jest puste!")
    else:
        while top.left is not None:
            top = top.left
        return top
