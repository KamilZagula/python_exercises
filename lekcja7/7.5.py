import unittest
from circles import Circle

class TestCircle(unittest.TestCase):

	def setUp(self):
		self.circle = Circle(radius = 2)

	def test_init(self):
		with self.assertRaises(ValueError):
			test = Circle(12, 12, -1)
		try:
			test2 = Circle(12, 12, 12)
		except ValueError:
			self.fail("Circle(12, 12, 12) raises ValueError")

	def test_repr(self):
		self.assertEquals("Circle(0, 0, 2)", self.circle.__repr__())

	def test_cmp(self):
		self.assertTrue(self.circle == self.circle)
		self.assertTrue(self.circle != Circle(radius = 3))

	def test_area(self):
		self.assertEquals(12.5663706144, round(self.circle.area(), 10))
		self.assertEquals(452.389342117, round(Circle(radius = 12).area(), 9))

	def test_move(self):
		with self.assertRaises(ValueError):
			self.circle.move("a", 1.2)

		self.circle.move(2, 1)
		self.assertEquals(2, self.circle.pt.x)
		self.assertEquals(1, self.circle.pt.y)
		circle2 = Circle(12, 3, 1)
		circle2.move(-12, 1)
		self.assertEquals(0, circle2.pt.x)
		self.assertEquals(4, circle2.pt.y)
		self.assertEquals(1, circle2.radius)
		circle3 = Circle(0, 0, 1)
		circle3.move(1.1, 1.1)
		self.assertEquals(1.1, circle3.pt.x)
		self.assertEquals(1.1, circle3.pt.y)

	def test_cover(self):
		cover_circle = Circle(1, 2, 1).cover(Circle(5, 2, 2))
		self.assertEquals("Circle(3.5, 2, 3.5)", cover_circle.__repr__())
		cover_circle = Circle(1, 2, 1).cover(Circle(-2, -2, 1))
		self.assertEquals("Circle(-0.5, 0, 3)", cover_circle.__repr__())
		with self.assertRaises(ValueError):
			cover_circle = self.circle.cover("aaaa")

	def tearDown(self):
		pass


if __name__ == "__main__":
	unittest.main()