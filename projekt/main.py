#!/usr/bin/python

import sys
from settings import *
from DbController import *

"""
	Shows help
"""
def help():
    print "Program jest prosta implementacja \"bazy danych\""
    print "Pozwala na stworzenie w dowolnej lokalizacji bazy danych"
    print "dodanie do niej tabel, wstawianie, usuwanie i wyswietlenie wszystkich wierszy"
    print "Flagi:"
    print "-h / --help - wyswietla pomoc"
    print "-o <nazwa> / --open <nazwa> - otworzenie bazy danych lub utworzenie w przypadku gdy plik nie istnieje"
    print "-l / --location - wyswietla biezaca lokalizacje baz danych"
    print "-nl <location>/ --newlocation <location> - zmienia biezaca lokalizacje baz danych"

"""
	Shows current database location
"""
def showLocation():
    settings = Settings()
    print "Current databases location:", settings.getDbsDirectory()

"""
	Changes current database location if possible
"""
def changeLocation(location):
    settings = Settings()
    if settings.isLocationPossible(location):
        settings.changeDbsDirectory(location)
        print "New location is changed to", settings.getDbsDirectory()
    else:
        print "New location is not available"

"""
	Shows possible commands
"""
def showPossibleCommands():
    print
    print "Komendy:"
    print "createTable <nazwa> <columns> - tworzy tabele o podanej nazwie z podanymi kolumnami, kolumny powinny byc oddzielone przecinkiem"
    print "dropTable <nazwa> - usuwa podana tabele"
    print "tableExists <nazwa> - sprawdza czy podana tabela istnieje"
    print "selectAll <nazwa> - wyswietla wszystkie wartosci w tabeli"
    print "insert <nazwa> <values> - wstawia do tabeli nowy wiersz, values powinny byc w formie kolumna:wartosc,kolumna2:wartosc2"
    print "delete <nazwa> <values> - usuwa z podanej tabeli wszystkie wiersze, ktore spelniaja warunki values, values powinny byc w formie kolumna:wartosc,kolumna2:wartosc2"
    print "exit - aby zakonczyc dzialanie programu"
    print

"""
	Opens database and waits for user inputs
"""
def openAndWait(table_name):
    controller = DbController(table_name)
    if controller.isDbCreated():
        print "Baza danych", table_name, "zostala stworzona"
    else:
        print "Otworzono baze danych", table_name
    print "Aby wyswietlic komendy wpisz: help"
    waitForInput(controller)

"""
 	Waits for given input until user types 'exit'
"""
def waitForInput(controller):
    try:
        while True:
            action = raw_input(">> ")
            if action == "exit":
                sys.exit(0)
            else:
                handleCommand(controller, action)
    except KeyboardInterrupt:
        sys.exit(0)

"""
	Handles given action
"""
def handleCommand(controller, action):
    params = action.split(" ")
    command = params[0]
    if command == "createTable" and len(params) == 3:
        try:
            controller.createTable(params[1], params[2].split(","))
            print "Stworzono tabele",params[1]
        except DatabaseError:
            print "Tabela", params[1], "juz istnieje"
    elif command == "tableExists" and len(params) == 2:
        if controller.tableExists(params[1]):
            print "Tabela", params[1], "istnieje"
        else:
            print "Tabela", params[1], "nie istnieje"
    elif command == "dropTable" and len(params) == 2:
        try:
            controller.dropTable(params[1])
            print "Usunieto tabele", params[1]
        except DatabaseError:
            print "Tabela", params[1],"juz nie istnieje"
    elif command == "selectAll" and len(params) == 2:
        try:
            output = controller.selectAll(params[1])
            for row in output:
                text = "|".join(row)
                print text
                print "-"*len(text)
        except DatabaseError:
            print "Tabela", params[1], "nie istnieje"
    elif command == "insert" and len(params) == 3:
        keyValues = params[2].split(",")
        keyValues = [x.split(":") for x in keyValues]
        values = dict()
        for pair in keyValues:
            values.update({pair[0]: pair[1]})
        try:
            controller.insert(params[1], values)
            print "Wiersz dodany"
        except DatabaseError:
            print "Tabela", params[1], "nie istnieje"
    elif command == "delete" and len(params) == 3:
        keyValues = params[2].split(",")
        keyValues = [x.split(":") for x in keyValues]
        values = dict()
        for pair in keyValues:
            values.update({pair[0]: pair[1]})
        try:
            controller.deleteRows(params[1], values)
            print "Wiersze zostaly usuniete"
        except DatabaseError:
            print "Tabela", params[1], "nie istnieje"
    elif command == "help":
        showPossibleCommands()
    else:
        print "Nieznana akcja"

if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == "-h" or sys.argv[1] == "--help":
            help()
        elif (sys.argv[1] == "-o" or sys.argv[1] == "--open") and len(sys.argv) == 3:
            openAndWait(sys.argv[2])
        elif sys.argv[1] == "-l" or sys.argv[1] == "--location":
            showLocation()
        elif (sys.argv[1] == "-nl" or sys.argv[1] == "--newlocation") and len(sys.argv) == 3:
            changeLocation(sys.argv[2])
        else:
            help()
    else:
        help()