from settings import *
import os.path, os

class DbController:

    def __init__(self, db_name):
        self.settings = Settings()
        self.db_dir = self.settings.getDbsDirectory() + "/" + db_name
        self.db_exists = True
        if not os.path.exists(self.db_dir):
            self.db_exists = False
            os.makedirs(self.db_dir)

    """
        Checks if database was created or not. 
        Returns true if database was created by contructor.
    """
    def isDbCreated(self):
        return not self.db_exists

    """
        Deletes rows in given table by key-value.
        Key-Value: Key is a column name.
    """
    def deleteRows(self, table_name, keyValue):
        table = self.db_dir + "/" + table_name
        columns = self.getTableColumns(table_name)
        toRemove = dict()
        for key, value in keyValue.iteritems():
            toRemove.update({columns.index(key): value})
        with open(table, "r+") as fp:
            lines = fp.readlines()
            lines = [x.strip() for x in lines]
            lines = [x.split(",") for x in lines]
            fp.seek(0)
            for i in lines:
                lineToRemove = True
                for key, value in toRemove.iteritems():
                    if i[key] != value:
                        lineToRemove = False
                        break
                if not lineToRemove:
                    fp.write(self.__getStringFromArray(i))
            fp.truncate()

    def __getStringFromArray(self, array):
        output = ""
        for a in array:
            output += a
            output += ","
        output = output[:-1]
        output += "\n"
        return output

    """ 
        Inserts the row for table_name 
        keyValue - dict with column name as key and value
    """
    def insert(self, table_name, keyValue):
        table = self.db_dir + "/" + table_name
        columns = self.getTableColumns(table_name)
        row = ""
        for column in columns:
            try:
                row += str(keyValue[column])
                row += ","
            except KeyError:
                row += " "
                row += ","
        row = row[:-1]
        with open(table, "a") as fp:
            fp.write(row + "\n")

    """
        Gets table columns as array.
    """
    def getTableColumns(self, table_name):
        table = self.db_dir + "/" + table_name
        if not os.path.exists(table):
            raise DatabaseError("Table " + table_name + " doesn't exists")
        with open(table, "r") as fp:
            return fp.readline().strip().split(",")

    """
        Selects all rows from given table.
    """
    def selectAll(self, table_name):
        table = self.db_dir + "/" + table_name
        if not os.path.exists(table):
            raise DatabaseError("Table "+ table_name + " doesn't exists")
        with open(table, "r") as fp:
            content = fp.readlines()
            content = [x.strip() for x in content] 
            content = [x.split(",") for x in content]
            output = []
            for row in content:
                newRow = []
                for value in row:
                    if value == " ":
                        newRow.append(None)
                    else:
                        newRow.append(value)
                output.append(newRow)
            return output

    """
        Creates table with given name and columns.
        Columns must be separated by comma.
    """
    def createTable(self, table_name, columns):
        table = self.db_dir + "/" + table_name
        if os.path.exists(table):
            raise DatabaseError("Table "+ table_name +" already exists")

        with open(table, "w+") as fp:
            for index in range(len(columns)):
                fp.write(columns[index])
                if index < len(columns) - 1:
                    fp.write(",")
                else:
                    fp.write("\n")

    """
        Removes given table.
    """
    def dropTable(self, table_name):
        table = self.db_dir + "/" + table_name
        if not os.path.exists(table):
            raise DatabaseError("Table "+ table_name + " doesn't exists")
        os.remove(table)

    """
        Checks if given table exists.
    """
    def tableExists(self, table_name):
        table = self.db_dir + "/" + table_name
        return os.path.exists(table)

class DatabaseError(Exception):

    def __init__(self, message):
        super(DatabaseError, self).__init__(message)